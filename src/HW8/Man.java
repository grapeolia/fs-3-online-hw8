package HW8;

import java.util.Map;

public final class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<String,String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void repairCar(){
        System.out.println("Finally! I've finished repairing my car.");
    }


    @Override
    public String toString() {
        return "Man{" +
                "name='" + super.getName() + '\'' +
                ", surname='" + super.getSurname() + '\'' +
                ", year=" + super.getYear() +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() +
                '}';
    }

}
