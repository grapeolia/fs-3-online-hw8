package HW8;

import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {

        Set<String> petHabits = Set.of("eat","sleep","jump");
        Pet simpleDomesticCat = new DomesticCat();
        Pet fullDog = new Dog("Tofik",2,34,petHabits);

        Man petrenkoFather = new Man("Father", "Petrenko", 1974);
        Woman petrenkoMother = new Woman("Mother", "Petrenko", 1979,78,setUpDailySchedule());
        Family petrenkoFamilyWithoutChildren = new Family(petrenkoMother,petrenkoFather);

        Man sydorenkoFather = new Man("Father", "Sydorenko", 1974);
        Woman sydorenkoMother = new Woman("Mother", "Sydorenko", 1979,78,setUpDailySchedule());
        Human sydorenkoBoyChild = new Human("childBoy","Sydorenko",2004);
        List<Human> sydorenkoChildren = Arrays.asList(sydorenkoBoyChild);
        Set<Pet> sydorenkoPets = Set.of(simpleDomesticCat);
        Family sydorenkoFamily = new Family(sydorenkoMother,sydorenkoFather,sydorenkoChildren,sydorenkoPets);

        Man chumakFather = new Man();
        Woman chumakMother = new Woman("Mother","Chumak",1996);
        Human chumakChild2 = new Human();
        Human chumakChild3 = new Human("Child3", "Chumak", 2022);
        Family chumakFamily = new Family();



        List<Family> families = new ArrayList<>(List.of(
                petrenkoFamilyWithoutChildren,
                sydorenkoFamily,
                chumakFamily
        ));
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.start();

    }

    public static Map<String,String> setUpDailySchedule(){
        Map<String,String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY.name(),"Get Enough Sleep");
        schedule.put(DayOfWeek.TUESDAY.name(),"Rise Early");
        schedule.put(DayOfWeek.WEDNESDAY.name(),"Meditate");
        schedule.put(DayOfWeek.THURSDAY.name(), "Workout");
        schedule.put(DayOfWeek.FRIDAY.name(),"Eat A Good Breakfast");
        schedule.put(DayOfWeek.SATURDAY.name(),"Take A Nap");
        schedule.put(DayOfWeek.SUNDAY.name(), "Take Breaks To Re-energize");

        return schedule;
    }

}