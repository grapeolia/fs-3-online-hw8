package HW8;

import java.util.Map;

public class Human {
    public Family family;
    public String name;
    public String surname;
    private int year;
    private int iq;
    private Map<String,String> schedule;

    public Human() {

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        setYear(year);
    }

    public Human(String name, String surname, int year, int iq, Map<String,String> schedule) {
        this.name = name;
        this.surname = surname;
        setYear(year);
        setIq(iq);
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if(year>=1900 && year<=2023){
            this.year = year;
        } else {
            System.out.println("Year has incorrect value. Must be 1900...2022. Year 1900 has been set by default.");
            this.year = 1900;
        }
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if(iq>=0 && iq<=100){
            this.iq = iq;
        } else {
            System.out.println("IQ has incorrect value. Must be 0..100. IQ 0 has been set by default.");
            this.iq = 0;
        }
    }

    public Family getFamily(){
        return family;
    }
    public Human getMother() {
       if(this.getFamily()!=null) {
           for(Human human : this.getFamily().getChildren()){
               if(this == human){
                   return this.getFamily().getMother();
               }
           }
       }
       System.out.println("I'm not a child in this family.");
       return null;
    }

    public Human getFather() {
        if(this.getFamily()!=null) {
            for(Human human : this.getFamily().getChildren()){
                if(this == human){
                    return this.getFamily().getFather();
                }
            }
        }
        System.out.println("I'm not a child in this family.");
        return null;
    }
    public Map<String,String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String,String> schedule) {
        this.schedule = schedule;
    }

    public void greetPets(){
        if (this.getFamily() != null && this.getFamily().getPets() != null) {
            String nickNames = "";
            for(Pet pet : this.getFamily().getPets()){
                if(pet.getNickname() != null && pet.getNickname() != "") {
                    nickNames = ", " + pet.getNickname();
                }
            }
            System.out.println("Hi, " + nickNames + "!");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname.");
        }
    }
    public void describePet(Pet myPet){
        if (this.getFamily().getPets() != null && this.getFamily().getPets().contains(myPet)) {
            for (Pet pet : this.family.getPets()) {
                if(pet == myPet){
                    String trickDescription = pet.getTrickLevel()>50
                            ? "very tricky"
                            : "barely tricky";
                    System.out.println("I've got a "+pet.getSpecies()+
                            ". It's "+pet.getAge()+
                            ", it's "+trickDescription+".");
                }
            }
        } else {
            System.out.println("I don't have this pet.");
        }
    }



    @Override
    public String toString() {
        String motherRecord = (family != null && family.getMother() != null)
                ? (family.getMother().getName()+" "+ family.getMother().getSurname())
                : "no record";
        String fatherRecord = (family != null && family.getFather() != null)
                ? (family.getFather().getName() + " " + family.getFather().getSurname())
                : "no record";

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }
}
